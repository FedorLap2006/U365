﻿#ifndef FIO_H
#define FIO_H

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>

void fill_file_struct(FILE*, int);

#endif
