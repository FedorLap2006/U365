int read(int, void*, int);
int write(int, void*, int);
const char* getenv(const char*);
void setenv(const char*, const char*);
int open(const char*, const char*);
int close(int);